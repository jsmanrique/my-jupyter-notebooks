# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 J. Manrique López de la Fuente
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#     J. Manrique López de la Fuente <jsmanrique@gmail.com>
#

import requests
import pandas as pd
from dateutil import parser

from perceval.backends.core.git import Git
from perceval.backends.core.github import GitHub

from threading import Thread
import time
from queue import Queue

import logging
# logging.basicConfig(level=logging.INFO)


def owner_repositories(name):
    """Function to get the
    self.github_token = github_token list of git repositories for a given GitHub organization or user
    Args:
      name (str): GitHub organization or user name
    Returns:
      list: List of git repositories URIs
    """
    query = "org:{}".format(name)
    page = 1
    repositories = []
    r = requests.get(
        'https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
    items = r.json()['items']
    while len(items) > 0:
        time.sleep(5)
        for item in items:
            repositories.append(
                {'owner': name, 'name': item['name'], 'url': item['clone_url']})
        page += 1
        r = requests.get(
            'https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
        try:
            items = r.json()['items']
        except:
            logging.info('Error getting the repos list')
            logging.info(r.json())

    logging.info('{} repositories: {}'.format(len(repositories), repositories))
    return repositories


class GitData:
    def __init__(self, repositories):
        """GitData init function
        Args:
          repositories (list): List of git repositories URIs
        Returns:
          pandas.Dataframe : Dataframe with git author, commit hash, repository 
                             name, and author commit date
        """
        dataframes = []
        threads = []
        que = Queue()
        for i, repo in enumerate(repositories):
            t = Thread(target=lambda q, arg1: q.put(
                self.git_repo_data(arg1)), args=(que, repo['url']))
            threads.append(t)
            t.start()
            logging.info('Adding {} of {} repositories to the qeue: {}'.format(
                i+1, len(repositories), repo['url']))

        for t in threads:
            t.join()

        i = 0
        while not que.empty():
            i = i + 1
            result = que.get()
            dataframes.append(result)

        logging.info(
            'Data from {} repositories gathered'.format(len(dataframes)))

        git_data = pd.concat(dataframes)
        
        git_data.set_index('date', inplace=True)
        git_data.index = pd.to_datetime(git_data.index, errors='coerce', utc=True)

        self.dataframe = git_data

    def git_repo_data(self, gitURI):
        """Function to get commits data from a git repository
        Args:
          gitURI (str): git repository URI
        Returns:
          paDataframe : Dataframe with git author, , github_tokencommit hash, 
                                repository name, and author commit date
        """

        git_repo = gitURI.split('/')[-1]
        data_repository = Git(uri=gitURI, gitpath='/tmp/{}'.format(git_repo))
        df = pd.DataFrame()

        for commit in data_repository.fetch():
            contributor_git_username = commit['data']['Author']
            contributor_name = contributor_git_username.split('<')[0][:-1]
            contributor_email = contributor_git_username.split('<')[
                1].split('>')[0]
            contributor_email_domain = contributor_git_username.split(
                '@')[-1][:-1]
            commit_date = parser.parse(commit['data']['AuthorDate'])
            df = df.append({
                'author': commit['data']['Author'],
                'name': contributor_name,
                'email': contributor_email,
                'email_domain': contributor_email_domain,
                'commit': commit['data']['commit'],
                'repository': git_repo,
                'date': commit['data']['AuthorDate'],
                'weekday': commit_date.isoweekday(),
                'hour': commit_date.hour,
            }, ignore_index=True)

        return df


class GitHubData:
    def __init__(self, repositories, github_token):
        """GitData init function
        Args:
          repositories (list): List of repositories

        Returns:
          pandas.Dataframe : Dataframe
        """
        self.github_token = github_token
        dataframes = []

        for repo in repositories:
            result = self.github_issues_repo_data(repo['owner'],repo['name'])
            dataframes.append(result)

        logging.info(
            'Data from {} repositories gathered'.format(len(dataframes)))

        gh_issues_data = pd.concat(dataframes)

        gh_issues_data.set_index('date', inplace=True)
        gh_issues_data.index = pd.to_datetime(gh_issues_data.index, errors='coerce', utc=True)

        self.dataframe = gh_issues_data

    def github_issues_repo_data(self, repoOwner, repoName):
        """Function to get issues (and pull requests) data from a GitHub repository
        Args:
          repoName (str): repository name
        Returns:
          pandas.Dataframe: Data frame with issues information
        """

        github_repo = GitHub(owner=repoOwner, repository=repoName,
                             api_token=self.github_token, sleep_for_rate=True, sleep_time=300, min_rate_to_sleep=10)
        df = pd.DataFrame()

        for issue in github_repo.fetch():
            created_at = issue['data']['created_at']
            # If the issue/pull-request is closed, we get the time to close it
            if issue['data']['state'] == 'closed':
                closed_at = issue['data']['closed_at']
                creation_date = parser.parse(created_at)
                closing_date = parser.parse(closed_at)
                delta_time = (closing_date - creation_date).total_seconds()
            else:
                delta_time = "None"
            summary = {
                'date': created_at,
                'author': issue['data']['user_data']['login'],
                'title': issue['data']['title'],
                'state': issue['data']['state'],
                'issue': issue['data']['html_url'],
                'comments': issue['data']['comments'],
                'time_to_solve': delta_time,
                'repository': repoOwner+'/'+repoName,
            }
            # We check if the item is an issue or pull request
            if 'pull_request' in issue['data'].keys():
                summary['issue_type'] = 'pull-request'
            else:
                summary['issue_type'] = 'issue'

            df = df.append(summary, ignore_index=True)

        return df
