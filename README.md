# My Jupyter Notebooks

These are some [Jupyter Notebooks](https://jupyter.org/) I've created for several reasons.

To run them you might need the Python libraries described in the 
[requirements.txt](requirements.txt) file.

Some of them are available through [MyBinder](https://mybinder.org/) project:
* [GitHub Analyzer](GitHub_Analyzer_NG.ipynb) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jsmanrique%2Fmy-jupyter-notebooks/master?filepath=GitHub_Analyzer_NG.ipynb)